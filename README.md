# iconsink
Here you will get collection of icons
<img src="https://media.iconsink.com/tag_images/design-development.jpg" alt="iconsink intro">
<a href="https://www.iconsink.com/">iconsink</a>
    
<a href="https://www.iconsink.com/basic">basic icons</a>

<img src="https://media.iconsink.com/tag_images/design-development.jpg" alt="design-development">
<a href="https://www.iconsink.com/design-development">design development icons</a>

<img src="https://media.iconsink.com/tag_images/clothes-fashion.jpg" alt="clothes-fashion">
<a href="https://www.iconsink.com/clothes-fashion">clothes fashion icons</a>

<img src="https://media.iconsink.com/tag_images/time-date.jpg" alt="time-date">
<a href="https://www.iconsink.com/time-date">time date icons</a>

<a href="https://www.iconsink.com/maps-location">maps location icons</a>

<img src="https://media.iconsink.com/tag_images/furniture.jpg" alt="furniture">
<a href="https://www.iconsink.com/furniture">furniture icons</a>

<img src="https://media.iconsink.com/tag_images/sport.jpg" alt="Sport">
<a href="https://www.iconsink.com/sport">Sport icons</a>

<img src="https://media.iconsink.com/tag_images/multimedia-gaming.jpg" alt="multimedia-gaming">
<a href="https://www.iconsink.com/multimedia-gaming">multimedia gaming icons</a>

<img src="https://media.iconsink.com/tag_images/touch-gestures.jpg" alt="touch-gestures">
<a href="https://www.iconsink.com/touch-gestures">touch gestures icons</a>

<img src="https://media.iconsink.com/tag_images/user-interface.jpg" alt="user-interface">
<a href="https://www.iconsink.com/user-interface">user interface icons</a>


<img src="https://media.iconsink.com/tag_images/travel.jpg" alt="travel">
<a href="https://www.iconsink.com/travel">travel icons</a>

<img src="https://media.iconsink.com/tag_images/arrows.jpg" alt="arrows">
<a href="https://www.iconsink.com/arrows">arrows icons</a>

<img src="https://media.iconsink.com/tag_images/weather.jpg" alt="weather">
<a href="https://www.iconsink.com/weather">weather icons</a>

<img src="https://media.iconsink.com/tag_images/business-finance.jpg" alt="business-finance">
<a href="https://www.iconsink.com/business-finance">business finance icons</a>

<a href="https://www.iconsink.com/social-icons">social icons icons</a>

<img src="https://media.iconsink.com/tag_images/health.jpg" alt="health-medical">
<a href="https://www.iconsink.com/health-medical">health medical icons</a>

<img src="https://media.iconsink.com/tag_images/shopping.jpg" alt="shopping">
<a href="https://www.iconsink.com/shopping">shopping icons</a>

<a href="https://www.iconsink.com/food">food icons</a>

<a href="https://www.iconsink.com/transport">transport icons</a>

<a href="https://www.iconsink.com/content-text-editing">content and text editing icons</a>

<a href="https://www.iconsink.com/files-folders">files and folders icons</a>

<a href="https://www.iconsink.com/emoticons">emoticons icons</a>

<a href="https://www.iconsink.com/sign-symbols">sign symbols icons</a>

<a href="https://www.iconsink.com/file-types">file types icons</a>

<a href="https://www.iconsink.com/user">user icons</a>

<a href="https://www.iconsink.com/cinema">cinema icons</a>

<a href="https://www.iconsink.com/icon/chevron-left-69">chevron left icons</a>

